package com.test.ideaware.dependencyinjection;

import com.test.ideaware.view.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
interface ActivityBuilder {

    @ContributesAndroidInjector(modules = ActivityModule.class)
    MainActivity bindMainActivity();
}
