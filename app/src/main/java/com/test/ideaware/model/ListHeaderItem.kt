package com.test.ideaware.model

import com.test.ideaware.R

class ListHeaderItem(
    val date: String
) :
    Item {

    override fun getType(): Int {
        return R.integer.list_header_item
    }
}
