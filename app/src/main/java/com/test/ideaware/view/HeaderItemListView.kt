package com.test.ideaware.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.test.ideaware.databinding.ListHeaderItemBinding
import com.test.ideaware.model.Item
import com.test.ideaware.model.ListHeaderItem
import com.test.ideaware.viewmodel.ListHeaderItemViewMvvm
import com.test.ideaware.viewmodel.ListHeaderItemViewViewModel


class HeaderItemListView : FrameLayout, ItemView, ListHeaderItemViewMvvm.View {

    private var binding: ListHeaderItemBinding? = null
    private lateinit var viewModel: ListHeaderItemViewMvvm.ViewModel

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context)
    }

    private fun init(context: Context) {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = DataBindingUtil.inflate(
            inflater,
            com.test.ideaware.R.layout.list_header_item,
            this,
            true
        )
        val lp = RecyclerView.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        rootView.layoutParams = lp
    }

    override fun bind(item: Item, position: Int) {
        viewModel = ListHeaderItemViewViewModel(item as ListHeaderItem)
        binding!!.viewmodel = viewModel
    }
}
