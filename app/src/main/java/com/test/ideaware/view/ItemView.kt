package com.test.ideaware.view

import com.test.ideaware.model.Item

interface ItemView {

    fun bind(item: Item, position: Int)
}
