package com.test.ideaware.view;

import com.test.ideaware.viewmodel.ItemsListViewMvvm;
import com.test.ideaware.viewmodel.providers.Injectable;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ItemsListViewFactory {

    @NotNull
    ItemsListViewMvvm.View getItemsListView(List<Injectable> injectables);
}
