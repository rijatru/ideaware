package com.test.ideaware.viewmodel

import androidx.lifecycle.ViewModel
import com.test.ideaware.model.ListHeaderItem

class ListHeaderItemViewViewModel(private var item: ListHeaderItem) : ViewModel(),
    ListHeaderItemViewMvvm.ViewModel {

    override fun getDate(): String? {
        return item.date
    }
}
